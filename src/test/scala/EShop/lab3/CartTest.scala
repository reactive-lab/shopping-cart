package EShop.lab3

import EShop.lab2.CartActor
import EShop.lab2.CartActor._
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

class CartTest
    extends TestKit(ActorSystem("CartTest"))
    with FlatSpecLike
    with ImplicitSender
    with BeforeAndAfterAll
    with Matchers
    with ScalaFutures {

  override def afterAll: Unit =
    TestKit.shutdownActorSystem(system)

  //use GetItems command which was added to make test easier
  it should "add item properly" in {
    val cart = TestActorRef(CartActor.props)
    val item = "Hamlet"
    cart ! AddItem(item)
    cart ! GetItems
    expectMsg(List(item))
  }

  it should "be empty after adding and removing the same item" in {
    val cart = TestActorRef(CartActor.props)
    val item = "Hamlet"
    cart ! AddItem(item)
    cart ! RemoveItem(item)
    cart ! GetItems
    expectMsg(List.empty)
  }

  it should "start checkout" in {
    val cart = TestActorRef(CartActor.props)
    cart ! AddItem("Hamlet")
    cart ! StartCheckout
    expectMsgPF() {
      case _: CheckoutStarted => ()
    }
  }
}
