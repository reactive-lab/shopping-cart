package EShop.lab3

import EShop.lab2.Checkout._
import EShop.lab2.{CartActor, Checkout}
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActor, TestActorRef, TestKit, TestProbe}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

class CheckoutTest
    extends TestKit(ActorSystem("CheckoutTest"))
    with FlatSpecLike
    with ImplicitSender
    with BeforeAndAfterAll
    with Matchers
    with ScalaFutures {

  override def afterAll: Unit =
    TestKit.shutdownActorSystem(system)

  it should "Send close confirmation to cart" in {
    val cart = TestProbe()
    val checkout = cart.childActorOf(Checkout.props(cart.ref))
    checkout ! StartCheckout
    checkout ! SelectDeliveryMethod("xD")
    checkout ! SelectPayment("xD")
    checkout ! ReceivePayment
    cart.expectMsg(CartActor.CloseCheckout)
  }

}
