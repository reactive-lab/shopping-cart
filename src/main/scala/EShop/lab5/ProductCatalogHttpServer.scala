package EShop.lab5

import EShop.lab5.ProductCatalog._
import akka.actor.ActorSystem
import akka.http.scaladsl.server.{HttpApp, Route}
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.ConfigFactory

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

class ProductCatalogHttpServer extends HttpApp {
  implicit private val timeout: Timeout = 10 seconds
  implicit private val ec: ExecutionContext = ExecutionContext.global

  override protected def routes: Route =
    path("items") {
      post {
        entity(as[GetItems]) { getItems =>
          complete {
            for {
              actor <- systemReference
                .get()
                .actorSelection("akka://ProductCatalog@127.0.0.1:2553/user/productcatalog")
                .resolveOne()
              response <- (actor ? getItems).mapTo[Items]
            } yield {
              response
            }
          }
        }
      }
    }
}

object ProductCatalogHttpServer extends App {
  def apply(): ProductCatalogHttpServer = new ProductCatalogHttpServer()

  val config = ConfigFactory.load()

  val system = ActorSystem("api", config.getConfig("api").withFallback(config))

  private val productCatalogSystem = ActorSystem(
    "ProductCatalog",
    config.getConfig("productcatalog").withFallback(config)
  )

  productCatalogSystem.actorOf(
    ProductCatalog.props(new SearchService()),
    "productcatalog"
  )

  ProductCatalogHttpServer().startServer("localhost", 9000, system)
}
