package EShop.lab4

import EShop.lab2.{Cart, Checkout}
import akka.actor.{ActorLogging, Cancellable, Props}
import akka.persistence.{PersistentActor, SnapshotOffer}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.Random
import scala.language.postfixOps

object PersistentCartActor {

  def props(persistenceId: String): Props = Props(new PersistentCartActor(persistenceId))
}

class PersistentCartActor(val persistenceId: String) extends PersistentActor with ActorLogging {

  import EShop.lab2.CartActor._

  implicit private val ec: ExecutionContext = context.system.dispatcher

  val cartTimerDuration: FiniteDuration = 5 seconds

  private def scheduleTimer: Cancellable = context.system.scheduler.scheduleOnce(cartTimerDuration, self, ExpireCart)

  override def receiveCommand: Receive = empty

  private def updateState(event: Event, timer: Option[Cancellable] = None): Unit = {
    timer.foreach(_.cancel)
    context become (event match {
      case CartExpired | CheckoutClosed | CartEmptied                       => empty
      case CheckoutCancelled(cart)                                          => nonEmpty(cart, scheduleTimer)
      case ItemAdded(item, cart)                                            => nonEmpty(cart.addItem(item), scheduleTimer)
      case ItemRemoved(item, cart) if cart.contains(item) && cart.size == 1 => empty
      case ItemRemoved(item, cart)                                          => nonEmpty(cart.removeItem(item), scheduleTimer)
      case CheckoutStarted(_, cart)                                         => inCheckout(cart)
    })
  }

  def empty: Receive = {
    case AddItem(item) => persist(ItemAdded(item, Cart.empty))(updateState(_))
    case GetItems      => sender ! Cart.empty
  }

  def nonEmpty(cart: Cart, timer: Cancellable): Receive = {
    case AddItem(item) =>
      persist(ItemAdded(item, cart))(updateState(_, Some(timer)))
    case RemoveItem(item) if cart.contains(item) && cart.size == 1 =>
      persist(CartEmptied)(updateState(_, Some(timer)))
    case RemoveItem(item) if cart.contains(item) =>
      persist(ItemRemoved(item, cart))(updateState(_, Some(timer)))
    case StartCheckout =>
      val checkoutRef = context.actorOf(PersistentCheckout.props(self, Random.alphanumeric.take(256).mkString))
      persist(CheckoutStarted(checkoutRef, cart)) { event =>
        checkoutRef ! Checkout.StartCheckout
        sender() ! event
        updateState(event, Some(timer))
      }
    case ExpireCart =>
      persist(CartExpired)(updateState(_, Some(timer)))
    case GetItems => sender ! cart
  }

  def inCheckout(cart: Cart): Receive = {
    case CloseCheckout  => persist(CheckoutClosed)(updateState(_))
    case CancelCheckout => persist(CheckoutCancelled(cart))(updateState(_))
  }

  override def receiveRecover: Receive = {
    case event: Event     => updateState(event)
    case _: SnapshotOffer => log.error("Received unhandled snapshot offer")
  }
}
