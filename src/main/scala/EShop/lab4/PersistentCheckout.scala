package EShop.lab4

import EShop.lab2.CartActor
import EShop.lab3.Payment
import akka.actor.{ActorLogging, ActorRef, Cancellable, Props}
import akka.persistence.{PersistentActor, SnapshotOffer}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.Random
import scala.language.postfixOps

object PersistentCheckout {

  def props(cartActor: ActorRef, persistenceId: String): Props =
    Props(new PersistentCheckout(cartActor, persistenceId))
}

class PersistentCheckout(cartActor: ActorRef, val persistenceId: String) extends PersistentActor with ActorLogging {

  import EShop.lab2.Checkout._

  implicit private val ec: ExecutionContext = context.system.dispatcher

  private def scheduleTimer = context.system.scheduler.scheduleOnce(timerDuration, self, Expire)

  val timerDuration: FiniteDuration = 1 second

  private def updateState(event: Event, maybeTimer: Option[Cancellable] = None): Unit = {
    val timer = maybeTimer.getOrElse(scheduleTimer)
    context become (event match {
      case CheckoutStarted           => selectingDelivery(timer)
      case DeliveryMethodSelected(_) => selectingPaymentMethod(timer)
      case CheckOutClosed            => closed
      case CheckoutCancelled         => cancelled
      case PaymentStarted(_)         => processingPayment(timer)
    })
  }

  def receiveCommand: Receive = {
    case StartCheckout => persist(CheckoutStarted)(updateState(_, Some(scheduleTimer)))
  }

  def selectingDelivery(timer: Cancellable): Receive = {
    case SelectDeliveryMethod(method) => persist(DeliveryMethodSelected(method))(updateState(_, Some(timer)))
    case CancelCheckout | Expire      => persist(CheckoutCancelled)(updateState(_, Some(timer)))
  }

  def selectingPaymentMethod(timer: Cancellable): Receive = {
    case SelectPayment(paymentMethod) =>
      timer.cancel()
      val paymentRef =
        context.actorOf(Payment.props(paymentMethod, sender(), self), Random.alphanumeric.take(256).mkString)
      persist(PaymentStarted(paymentRef)) { event =>
        sender() ! event
        updateState(event, Some(scheduleTimer))
      }
    case CancelCheckout | Expire => persist(CheckoutCancelled)(updateState(_, Some(timer)))
  }

  def processingPayment(timer: Cancellable): Receive = {
    case ReceivePayment =>
      timer.cancel()
      cartActor ! CartActor.CloseCheckout
      persist(CheckOutClosed)(updateState(_))
    case CancelCheckout | Expire => persist(CheckoutCancelled)(updateState(_))
  }

  def cancelled: Receive = {
    case _ => log.info("Checkout already cancelled")
  }

  def closed: Receive = {
    case _ => log.info("Checkout already closed")
  }

  override def receiveRecover: Receive = {
    case event: Event     => updateState(event)
    case _: SnapshotOffer => log.error("Received unhandled snapshot offer")
  }
}
