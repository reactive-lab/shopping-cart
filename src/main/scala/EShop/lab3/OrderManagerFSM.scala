package EShop.lab3

import EShop.lab2.{CartActor, CartFSM, Checkout}
import EShop.lab3.OrderManager._
import EShop.lab3.OrderManagerFSM._
import EShop.lab3.Payment.DoPayment
import akka.actor.{ActorRef, FSM}


object OrderManagerFSM {
  sealed trait State
  case object Uninitialized extends State
  case object Open          extends State
  case object InCheckout    extends State
  case object InPayment     extends State
  case object Finished      extends State


  sealed trait Data
  case object Empty                                                            extends Data
  case class CartData(cartRef: ActorRef)                                       extends Data
  case class CartDataWithSender(cartRef: ActorRef, sender: ActorRef)           extends Data
  case class InCheckoutData(checkoutRef: ActorRef)                             extends Data
  case class InCheckoutDataWithSender(checkoutRef: ActorRef, sender: ActorRef) extends Data
  case class InPaymentData(paymentRef: ActorRef)                               extends Data
  case class InPaymentDataWithSender(paymentRef: ActorRef, sender: ActorRef)   extends Data
}

class OrderManagerFSM extends FSM[State, Data] {

  startWith(Uninitialized, Empty)

  when(Uninitialized) {
    case Event(AddItem(id), Empty) =>
      val cartRef = context.actorOf(CartFSM.props())
      cartRef ! CartActor.AddItem(id)
      sender ! Done
      goto(Open) using CartData(cartRef)
  }

  when(Open) {

    case Event(AddItem(id), CartData(cartRef)) =>
      cartRef ! CartActor.AddItem(id)
      sender ! Done
      stay
    case Event(RemoveItem(id), CartData(cartRef)) =>
      cartRef ! CartActor.RemoveItem(id)
      sender ! Done
      stay
    case Event(Buy, CartData(cartRef)) =>
      cartRef ! CartActor.StartCheckout
      goto(InCheckout) using CartDataWithSender(cartRef, sender)
  }

  when(InCheckout) {
    case Event(CartActor.CheckoutStarted(checkoutRef, _), CartDataWithSender(_, sender)) =>
      sender ! Done
      stay using InCheckoutData(checkoutRef)
    case Event(SelectDeliveryAndPaymentMethod(delivery, payment), InCheckoutData(checkoutRef)) =>
      checkoutRef ! Checkout.SelectDeliveryMethod(delivery)
      checkoutRef ! Checkout.SelectPayment(payment)
      goto(InPayment) using InCheckoutDataWithSender(checkoutRef, sender)

  }

  when(InPayment) {
    case Event(Checkout.PaymentStarted(paymentRef), InCheckoutDataWithSender(_, sender)) =>
      sender ! Done
      goto(InPayment) using InPaymentData(paymentRef)
    case Event(Pay, InPaymentData(paymentRef)) =>
      paymentRef ! Payment.DoPayment
      stay using InPaymentDataWithSender(paymentRef, sender)
    case Event(Payment.PaymentConfirmed, InPaymentDataWithSender(_, sender)) =>
      sender ! Done
      goto(Finished)
  }

  when(Finished) {
    case _ =>
      sender ! "order manager finished job"
      stay()
  }

}
