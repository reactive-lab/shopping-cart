package EShop.lab3

import EShop.lab2.Checkout
import EShop.lab3.Payment._
import EShop.lab3.PaymentFSM._
import akka.actor.{ActorRef, FSM, Props}

object PaymentFSM {

  sealed trait Data
  case object Empty extends Data

  sealed trait State
  case object WaitingForPayment extends State
  case object PaymentReceived extends State

  def props(method: String, orderManager: ActorRef, checkout: ActorRef) =
    Props(new PaymentFSM(method, orderManager, checkout))

}

class PaymentFSM(method: String, orderManager: ActorRef, checkout: ActorRef) extends FSM[State, Data] {

  startWith(WaitingForPayment, Empty)

  when(WaitingForPayment) {
    case Event(DoPayment, Empty) =>
      orderManager ! PaymentConfirmed
      checkout ! Checkout.ReceivePayment
      goto(PaymentReceived)
  }

  when(PaymentReceived) {
    case _ =>
      log.info("payment already received")
      stay
  }

  onTransition {
    case WaitingForPayment -> PaymentReceived => context.stop(self)
  }

}
