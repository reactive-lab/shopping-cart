package EShop.lab3

import EShop.lab2.{CartActor, Checkout}
import EShop.lab3.OrderManager._
import akka.actor.{Actor, ActorRef, Props}

object OrderManager {

  sealed trait Command
  case class AddItem(id: String) extends Command
  case class RemoveItem(id: String) extends Command
  case class SelectDeliveryAndPaymentMethod(delivery: String, payment: String) extends Command
  case object Buy extends Command
  case object Pay extends Command

  sealed trait Ack
  case object Done extends Ack //trivial ACK

  def props = Props(new OrderManager)
}

class OrderManager extends Actor {

  override def receive: Receive = uninitialized

  def uninitialized: Receive = {
    case AddItem(item) =>
      val cartRef = context.actorOf(CartActor.props)
      cartRef ! CartActor.AddItem(item)
      sender ! Done
      context become open(cartRef)
  }

  def open(cartRef: ActorRef): Receive = {
    case AddItem(item) =>
      cartRef ! CartActor.AddItem(item)
      sender ! Done
    case RemoveItem(item) =>
      cartRef ! CartActor.RemoveItem(item)
      sender ! Done
    case Buy =>
      cartRef ! CartActor.StartCheckout
      context become inCheckout(cartRef, sender)
  }

  def inCheckout(cartActorRef: ActorRef, senderRef: ActorRef): Receive = {
    case CartActor.CheckoutStarted(checkoutRef, _) =>
      senderRef ! Done
      context become inCheckout(checkoutRef)
  }

  def inCheckout(checkoutActorRef: ActorRef): Receive = {
    case SelectDeliveryAndPaymentMethod(delivery, payment) =>
      checkoutActorRef ! Checkout.SelectDeliveryMethod(delivery)
      checkoutActorRef ! Checkout.SelectPayment(payment)
      context become inPayment(sender)
  }

  def inPayment(senderRef: ActorRef): Receive = {
    case Checkout.PaymentStarted(paymentRef) =>
      senderRef ! Done
      context become inPayment(paymentRef, senderRef)
  }

  def inPayment(paymentActorRef: ActorRef, senderRef: ActorRef): Receive = {
    case Pay =>
      paymentActorRef ! Payment.DoPayment
      context become inPayment(paymentActorRef, sender)
    case Payment.PaymentConfirmed =>
      senderRef ! Done
      context become finished
  }

  def finished: Receive = {
    case _ => sender ! "order manager finished job"
  }
}
