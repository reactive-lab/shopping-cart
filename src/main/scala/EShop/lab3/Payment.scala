package EShop.lab3

import EShop.lab2.Checkout
import Payment._
import akka.actor.{Actor, ActorLogging, ActorRef, Props}

object Payment {

  sealed trait Command
  case object DoPayment extends Command

  sealed trait Event
  case object PaymentConfirmed extends Event


  def props(method: String, orderManager: ActorRef, checkout: ActorRef) =
    Props(new Payment(method, orderManager, checkout))

}

class Payment(
  method: String,
  orderManager: ActorRef,
  checkout: ActorRef
) extends Actor with ActorLogging {

  override def receive: Receive = {
    case DoPayment =>
      orderManager ! PaymentConfirmed
      checkout ! Checkout.ReceivePayment
      context.stop(self)
  }

}
