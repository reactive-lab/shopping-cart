name := "EShop"

version := "0.2"

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  Library.akka,
  Library.akkaCluster,
  Library.akkaJackson,
  Library.akkaHttp,
  Library.akkaHttpSprayJson,
  Library.akkaPersistence,
  Library.akkaPersistenceQuery,
  Library.akkaPersistenceInMemory,
  Library.akkaRemote,
  Library.enumeratum,
  Library.leveldb,
  Library.leveldbjni,
  Library.akkaTestKit % Test,
  Library.scalaTest % Test
)

// scalaFmt
scalafmtOnCompile := true
