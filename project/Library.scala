import sbt._

object Library {

  object Version {
    val akka                    = "2.6.0"
    val akkaHttp                = "10.1.10"
    val akkaHttpCirce           = "1.29.1"
    val akkaPersistenceInMemory = "2.5.15.2"
    val cats                    = "2.0.0"
    val circe                   = "0.12.3"
    val enumeratum              = "1.5.13"
    val enumeratumCirce         = "1.5.22"
    val leveldb                 = "0.9"
    val leveldbjni              = "1.8"
    val shapeless               = "2.3.3"
    val scalaTest               = "3.0.8"
  }
  val akka                    = "com.typesafe.akka"         %% "akka-actor"                % Version.akka
  val akkaCluster             = "com.typesafe.akka"         %% "akka-cluster"              % Version.akka
  val akkaHttp                = "com.typesafe.akka"         %% "akka-http"                 % Version.akkaHttp
  val akkaHttpCirce           = "de.heikoseeberger"         %% "akka-http-circe"           % Version.akkaHttpCirce
  val akkaHttpSprayJson       = "com.typesafe.akka"         %% "akka-http-spray-json"      % Version.akkaHttp
  val akkaJackson             = "com.typesafe.akka"         %% "akka-serialization-jackson"% Version.akka
  val akkaPersistence         = "com.typesafe.akka"         %% "akka-persistence"          % Version.akka
  val akkaPersistenceQuery    = "com.typesafe.akka"         %% "akka-persistence-query"    % Version.akka
  val akkaPersistenceInMemory = "com.github.dnvriend"       %% "akka-persistence-inmemory" % Version.akkaPersistenceInMemory
  val akkaRemote              = "com.typesafe.akka"         %% "akka-remote"               % Version.akka
  val akkaTestKit             = "com.typesafe.akka"         %% "akka-testkit"              % Version.akka
  val cats                    = "org.typelevel"             %% "cats-core"                 % Version.cats
  val circe                   = "io.circe"                  %% "circe-core"                % Version.circe
  val circeGeneric            = "io.circe"                  %% "circe-generic"             % Version.circe
  val circeParser             = "io.circe"                  %% "circe-parser"              % Version.circe
  val enumeratum              = "com.beachape"              %% "enumeratum"                % Version.enumeratum
  val enumeratumCirce         = "com.beachape"              %% "enumeratum-circe"          % Version.enumeratumCirce
  val leveldb                 = "org.iq80.leveldb"          % "leveldb"                    % Version.leveldb
  val leveldbjni              = "org.fusesource.leveldbjni" % "leveldbjni-all"             % Version.leveldbjni
  val shapeless               = "com.chuusai"               %% "shapeless"                 % Version.shapeless
  val scalaTest               = "org.scalatest"             %% "scalatest"                 % Version.scalaTest


}
